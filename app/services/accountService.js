﻿wzimder.service("account", ['$http', 'constants', function ($http, constants, $localStorage) {
    var account = {};

    account.register = function (credentials, success, error) {
        $http({
            url: constants.apiBaseUrl + 'account/register',
            dataType: 'json',
            method: 'POST',
            unique: true,
            skipAuthorization: true,
            data: credentials
        }).success(function (data, status) {
            success();
        })
            .error(function (data, status) {
                error(data);
            });
    };

    account.getInfo = function (success) {
        $http.get(constants.apiBaseUrl + 'account')
        .success(function (data) {
            success(data)
        });
    };

    account.changeLocation = function (location, success, error) {
        $http.post(constants.apiBaseUrl + 'account/location', location)
        .success(function (data, status) {
            success();
        })
        .error(function (data, status) {
            error();
        });
    };

    account.changePassword = function (passwordData, success, error) {
        $http.post(constants.apiBaseUrl + 'account/changepassword', passwordData)
        .success(function (data, status) {
            success();
        })
        .error(function (data, status) {
            error();
        });
    }

    account.changeDescription = function (description, succes, error) {
        $http.post(constants.apiBaseUrl + 'account/description', description)
        .success(function (data, status) {
            succes();
        })
        .error(function () {
            error();
        });
    };

    account.setProfilePhoto = function (file, success,error) {
        var fd = new FormData();
        fd.append('file', file);
        $http.post(constants.apiBaseUrl + 'Photos', fd, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        })
        .success(success)
        .error(error);
    };

    //account.getPhotoUrl = function (id) {
    //    if (id === 0)
    //        return {'background-image': "url('img/empty.png')"};
    //    return {'background-image': "url(' + constants.apiBaseUrl + 'photos/' + id + ')"};
    //};

    account.getPhotoUrl = function (id) {
        if (id === 0)
            return { 'background-image': "url('img/empty.png')" };
        return { 'background-image': "url(" + constants.apiBaseUrl + 'photos/' + id + ")" };
    }

    account.getPhotoUrlCss = function (id) {
        if (id === 0)
            return "background-image: url('img/empty.png')";
        return "background-image: url(" + constants.apiBaseUrl + 'photos/' + id + ")";
    };
	
	account.getPhotoSrc = function(id) {
		if (id === 0)
			return 'img/empty.png';
		return constants.apiBaseUrl + 'photos/' + id;
	};


    return account;
}]);