﻿/// <reference path="../app.js" />
wzimder.service("pairsService", ['$http', 'constants', function ($http, constants) {
    var pairsService = {};

    pairsService.getPairs = function (success) {
        $http.get(constants.apiBaseUrl + 'pairs')
            .success(function (data) {
                success(data)
            });
    }

    pairsService.pairDecision = function (params) {
        $http.post(constants.apiBaseUrl + 'pairs', params)
            .success(function (data) {
                //success(data)
            });
    }

    return pairsService;
}]);