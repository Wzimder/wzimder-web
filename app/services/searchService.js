/// <reference path="../app.js" />
wzimder.service("searchService", ['$http', 'constants', function ($http, constants) {
    var search = {};
		
    search.setCriteria = function (criteria) {
        $http.put(constants.apiBaseUrl + 'searchoptions', criteria)
            .success(function (data, status) {
            })
            .error(function (data, status) {
                //error();
            });
    }

    search.searchUsers = function (success) {
        $http.get(constants.apiBaseUrl + 'users')
            .success(function (data) {
                success(data)
            });
    }

    search.getUser = function (userId, success) {
        $http.get(constants.apiBaseUrl + 'users/' + userId)
            .success(function (data) {
                success(data)
            });
    }

    search.getCriteria = function (success) {
        $http.get(constants.apiBaseUrl + 'searchoptions')
            .success(function (data) {
                success(data)
            });
    }

    return search;
}]);