﻿angular.module('wzimder').factory('httpRequestInterceptor',
['$q','$localStorage', function ($q, $localStorage) {
    return {
        'request': function (config) {
            if ($localStorage.userInfo != undefined) {
                config.headers.Authorization = 'Bearer ' + $localStorage.userInfo.access_token;
            }
            return config;
        }
    };
}]);