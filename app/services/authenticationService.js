﻿/// <reference path="../app.js" />
wzimder.factory('Auth', ['$http', 'constants','$localStorage', function ($http,constants, $localStorage) {
    var auth = {};

    function validateUser(credentials,success,error) {

        $http({
            url: constants.apiBaseUrl + 'account/login',
            dataType: 'jsonp',
            method: 'POST',
            unique: true,
            skipAuthorization: true,
            transformRequest: function(obj){
                return "grant_type=password&username="+ obj.email + "&password=" + obj.password;
            },
            data: credentials,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        })
            .success(function (data,status) {
                 console.log("successfull authentication")
                  $localStorage.userInfo = data;
                  auth.isUserLoggedIn = true;
                  success(data);
            })
            .error(function(data,status) {
                console.log("authentication failed")
                error(data);
            });
    }

    auth.login = function (credentials,success,error) {
        validateUser(credentials,success,error);
    };

    auth.logout = function()
    {
        delete $localStorage.userInfo;
    };

    auth.currentUser = function()
    {
    	if ($localStorage.userInfo != undefined) {
    		return $localStorage.userInfo.userName;
    	}
    	return null;
    };

    auth.getToken = function()
    {
        if ($localStorage.userInfo != undefined) {
            return $localStorage.userInfo.access_token;
        }
        return null;
    }

    auth.isUserLoggedIn = function()
    {
    	if ($localStorage.userInfo != undefined)
    	{
    		return true;
    	}
    	return false;
    };

    return auth;
}]);