﻿/// <reference path="../app.js" />
wzimder.service("chatService",['$http','constants', function ($http, constants) {
    var chatService = {};

    chatService.sendMessage = function (params) {
        $http.post(constants.apiBaseUrl + 'messages', params)
            .success(function (data) {
                //success(data)
            });
    }

    chatService.getNewMessages = function (userID, success) {
        console.log(userID);
        console.log(constants.apiBaseUrl + 'messages/' + userID);
        $http.get(constants.apiBaseUrl + 'messages/' + userID)
            .success(function (data) {
                console.log("Z Service");
                console.log(data);
                success(data)
            });
    }

    return chatService;
}]);