﻿/// <reference path="../app.js" />
wzimder.controller("chat", ['$scope', '$routeParams', '$interval', 'chatService', 'pairsService', 'searchService', 'account',
    function ($scope, $routeParams, $interval, chatService, pairsService, searchService, account) {

    //Wyłączenie robienia nowej linii w TextArea po naciśnięciu entera
    $(document).ready(function () {
        $("#ta_msg").keypress(function (event) {
            if (event.which == '13') {
                return false;
            }
        });
    });      

    //Pobranie UserId z URL
    if ($routeParams.userId !== null && $routeParams.userId !== undefined) {
        $scope.userId = $routeParams.userId;
        console.log("Wziąłęm userId = " + $scope.userId);
    }


    var intervalPromise;
    if ($routeParams.userId !== undefined)
        intervalPromise = $interval(refreshMessages, 8000);
    else if(intervalPromise !== undefined)
        $interval.stop(intervalPromise);   

    $scope.pairs = [];
    pairsService.getPairs(getPairs);

    //Pobranie par użytkownika
    function getPairs(data) {
        $scope.pairs = data;
    }

    if ($scope.userId !== undefined) {
        chatService.getNewMessages($scope.userId, getMessages);
        account.getInfo(getCurrentUser);
        searchService.getUser($scope.userId, getFriend);
    }

    function getCurrentUser(data) {
        $scope.currentUser = data;
    }

    //Pobranie informacji o userze, z którym piszemy
    function getFriend(data) {
        $scope.friend = data;        
    }

    //Pobranie nowych wiadomości, po otworzeniu czatu
    function getMessages(data) {
        console.log("Po otworzeniu czatu");
        console.log(data);
        if (data.length != 0) {
            $scope.messages = data;
            setScroll();
        }
        else if ($scope.messages === undefined)
            $scope.messages = [];

        //console.log($scope.messages);
    }

    //Pobieranie cykliczne nowych wiadomości
    function refreshMessages() {
        if ($routeParams.userId !== undefined) {
            //console.log("INTERVAL " + $routeParams.userId + ", Scope: " + $scope.userId);
            chatService.getNewMessages($scope.userId, getMessagesRefresh);
        }        
    }

    function getMessagesRefresh(data) {
        console.log("Po refreshu");
        console.log(data);
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                $scope.messages.push({
                    Text: data[i].Text,
                    SenderId: data[i].SenderId
                });
            }
            setScroll();
        }
    }

    $scope.sendMessage = function () {
        if ($scope.newMessage.replace(" ", "") != "") {
            $scope.newMessage = $scope.newMessage.trim();
            chatService.sendMessage({ Text: $scope.newMessage, RecipientId: $scope.userId });
            $scope.messages.push({
                Text: $scope.newMessage,
                SenderId: null
            });
            setScroll();
            //console.log($scope.messages);

            $scope.newMessage = "";
        }
    }

    $scope.getPhotoUrl = function (id) {
        return account.getPhotoUrl(id);
    }

    function setScroll() {
        var objDiv = document.getElementById("chat");
        objDiv.scrollTop = objDiv.scrollHeight;
    }
}]);