﻿/// <reference path="../app.js" />
wzimder.controller("application", ['$scope', 'Auth', '$location', function ($scope, Auth, $location) {
    $scope.user = Auth.currentUser();

    $scope.setCurrentUser = function (user) {
        $scope.user = user;
    }

    $scope.logout = function () {
        $scope.user = null;
        Auth.logout();
        $location.path('/');
    }

    $scope.isLoggedin = function () {
        return $scope.user != null ? true : false;
    }

    $scope.calculateAge = function calculateAge(birthday) { // birthday is a date
        var birthdayDate = new Date(birthday);
        var ageDifMs = Date.now() - birthdayDate.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }
}]);