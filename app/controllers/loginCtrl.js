﻿/// <reference path="../app.js" />
wzimder.controller("login", ['$scope', 'Auth', '$location', function ($scope, Auth, $location) {
    $scope.message = '';
    $scope.credentials = {};

    function successfullAuth(response)
    {
        $scope.setCurrentUser(response.userName);
        $location.path('/dashboard')
    }

    function failedAuth(data)
    {
        $scope.message = data.error_description;
        $location.path('/');
    }

    $scope.login = function()
    {
        Auth.login($scope.credentials, successfullAuth, failedAuth);
    }

    $scope.logout = function()
    {
        $scope.removeCurrentUser();
        Auth.logout();
    }

    $scope.register = function () {
        $location.path('/register');
    }
}]);