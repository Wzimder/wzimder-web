/// <reference path="../app.js" />
wzimder.controller("pairs", ['$scope', '$location', 'pairsService', 'searchService', 'account',
    function ($scope, $location, pairsService, searchService, account) {
    
    $scope.pairs = [];

    $scope.criteria = {};
    $scope.criteria.MinAge = 16;
    $scope.criteria.MaxAge = 60;
    $scope.criteria.Gender = 1;
    $scope.criteria.MaxDistanceKM = 100;

    pairsService.getPairs(getPairs);
    searchService.getCriteria(getCriteria);

    function getPairs(data) {
        console.log(data);
        $scope.pairs = data;
    }

    function getCriteria(data) {
        $scope.criteria = data;
    }

    $scope.searchUsers = function () {
        searchService.setCriteria(
            {
                MinAge: $scope.criteria.MinAge,
                MaxAge: $scope.criteria.MaxAge,
                Gender: $scope.criteria.Gender,
                MaxDistanceKM: $scope.criteria.MaxDistanceKM
            });

        $location.path('/search');
    }

    $scope.getPhotoUrl = function (id) {
         return account.getPhotoUrl(id);
    }

}]);