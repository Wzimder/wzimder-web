﻿/// <reference path="../app.js" />
wzimder.controller("register", ['$scope', 'account', '$location', 'Auth', function ($scope, account, $location, Auth) {

    $scope.credentials = {};
    $scope.credentials.gender = 1;
    $scope.message = "";
    $scope.validation = {};


    $scope.login = function()
    {
        $location.path('/');
    }

    function successfulRegistration() {
        $location.path('/');
    }
    function failedRegistration(data) {
        var v = data[Object.keys(data)[1]];
        setValidationResults(v);
        $scope.message = "Rejestracja nie powiodła się";
    }

    function setValidationResults(data) {
        for (var property in data) {
            if (Array.isArray(data[property])) {
                for (var element in data[property]) {
                    var elementValue = data[property][element];
                    $scope.validation[elementValue.substring(0, elementValue.indexOf(" "))] = elementValue.substring(elementValue.indexOf(" ") + 1);
                }
            }
            else {
                $scope.validation[property.substring(property.indexOf(".") + 1)] = data[property][0];
            }
        }
    }

    $scope.register = function () {
        if ($scope.credentials.password != $scope.credentials.confirmPassword) {
            $scope.message = "Hasła nie są takie same";
            return;
        }
        account.register($scope.credentials, successfulRegistration, failedRegistration);
    };

}]);