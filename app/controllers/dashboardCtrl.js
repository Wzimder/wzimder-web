﻿/// <reference path="../app.js" />
wzimder.controller('dashboard', ['$scope', '$location', 'account', 'searchService', function ($scope, $location, account, searchService) {
    
    var latitude;
    var longitude;
    getLocation();

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        }
    }

    function showPosition(position) {
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;

        account.changeLocation({ Location: latitude + ';' + longitude }, successLocationChange, locationChangeFailed);
    }

    $scope.userInfo = {};
    account.getInfo(getUserData);

    $scope.criteria = {};
    $scope.criteria.MinAge = 16;
    $scope.criteria.MaxAge = 60;
    $scope.criteria.Gender = 1;
    $scope.criteria.MaxDistanceKM = 100;

    function getUserData(data) {
        $scope.userInfo = data;
    }

    searchService.getCriteria(getCriteria);

    function getCriteria(data) {
        $scope.criteria = data;
    }

    $scope.searchUsers = function () {
        searchService.setCriteria(
            {
                MinAge: $scope.criteria.MinAge,
                MaxAge: $scope.criteria.MaxAge,
                Gender: $scope.criteria.Gender,
                MaxDistanceKM: $scope.criteria.MaxDistanceKM
            });

        $location.path('/search');
    }


    // ************************* //

    function successLocationChange() {
        console.log("Location set to: " + latitude + ";" + longitude);
    }

    function locationChangeFailed() {
        console.log("Location set FAILED to: " + latitude + ";" + longitude);
    }

}]);