﻿/// <reference path="../app.js" />
wzimder.controller("profile", ['$scope', 'account', function ($scope, account) {
    $scope.userInfo = {};
    $scope.location = '';
    $scope.password = {};
    $scope.description = $scope.userInfo.Description;
    $scope.photo = {};
    $scope.profilePhoto = {};
    account.getInfo(getUserData);

    function getUserData(data) {
        $scope.userInfo = data;
    }

    function successPhotoDownload(data) {
        $scope.profilePhoto = data;
    }

    function downloadingPhotoFailed() {
        //alert("Downloading photo failed!");
    }

    function successLocationChange() {
        alert("Location changed!");
    }

    function locationChangeFailed() {
        alert('Location change failed!');
    }

    $scope.changeLocation = function (location) {
        account.changeLocation({ Location: location }, successLocationChange, locationChangeFailed);
    }

    function successPasswordChange() {
        alert('Password changed!');
        $scope.password = {};
    }

    function passwordChangedFailed() {
        alert('Password change failed!');
    }

    $scope.changePassword = function (password) {
        account.changePassword(password, successPasswordChange, passwordChangedFailed);
    };

    function successDescriptionChange() {
        alert('Description chnaged!');
    };

    function descriptionChangeFailed() {
        alert('Description change failed!');
    };

    $scope.changeDescription = function (description) {
        account.changeDescription({ Description: description }, successDescriptionChange, descriptionChangeFailed)
    };

    function succesPhotoChange() {
        alert("Photo changed!");
    }

    function photoChangeFailed() {
        alert("Photo changed failed");
    }

    $scope.changePhoto = function () {
        account.setProfilePhoto($scope.photo, succesPhotoChange);
    }
	
	$scope.getPhotoUrl = function (id) {
        return account.getPhotoUrl(id);
    }

    $scope.getPhotoUrlCss = function (id) {
        return account.getPhotoUrlCss(id);
    }
	
	$scope.getPhotoSrc = function(id){
	console.log(id);
		return account.getPhotoSrc(id);
	}

}]);