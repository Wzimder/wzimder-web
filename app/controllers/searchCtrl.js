/// <reference path="../app.js" />
wzimder.controller("search", ['$scope', 'searchService', 'pairsService', 'account', function ($scope, searchService, pairsService, account) {   

    $scope.users = [];	

    searchService.searchUsers(searchSuccess);

    function searchSuccess(data) {
        console.log(data);
        $scope.users = data;  
		$scope.index = 0;
		if($scope.users.length > 0)
			$scope.crrentUserPhoto = "url(' " + account.getPhotoSrc($scope.users[$scope.index].Photo) + "')";
    }

    $scope.refuse = function () {
        pairsService.pairDecision({ UserId: $scope.users[$scope.index].Id, Status: 1 });
        incraseIndex();
    }

    $scope.accept = function () {
        pairsService.pairDecision({ UserId: $scope.users[$scope.index].Id, Status: 0 });
        incraseIndex();
    }
	
	/*$scope.getPhotoUrl = function (id) {
        return account.getPhotoUrl(id);
    }*/
	
	/*$scope.$watch("index", function() {
		$scope.getPhotoUrl = function () {
			console.log("**** " + $scope.users[$scope.index].Photo);
			return account.getPhotoUrl($scope.users[$scope.index].Photo);
		}
    });*/

    function incraseIndex() {
        if ($scope.index + 1 < $scope.users.length) {
            $scope.index++;
			$scope.crrentUserPhoto = "url(' " + account.getPhotoSrc($scope.users[$scope.index].Photo) + "')";
		}
        else
            $scope.users = [];
    }

}]);