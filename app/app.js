/// <reference path="libs/angular.min.js" />
var wzimder = angular.module("wzimder", ['ngRoute', 'ngStorage', '720kb.datepicker']);

wzimder.constant('constants', { apiBaseUrl: 'http://52.30.121.166/Backend/api/' })

wzimder.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/',
        {
            templateUrl: 'app/views/login.html',
            controller: 'login',
            access: {
                isLoginRequired: false
            }
        })
        .when('/error',
        {
            templateUrl: 'app/views/error.html',
            access: {
                isLoginRequired: false
            }
        })
        .when('/profile',
        {
            templateUrl: 'app/views/profilePage.html',
            controller: 'profile',
            access: {
                isLoginRequired: true
            }
        })
         .when('/dashboard',
        {
            templateUrl: 'app/views/dashboard.html',
            controller: 'dashboard',
            access: {
                isLoginRequired: true
            }
        })
        .when('/register',
        {
            templateUrl: 'app/views/register.html',
            controller: 'register',
            access:{
                isLoginRequired: false
            }
        })
        .when('/search',
        {
            templateUrl: 'app/views/search.html',
            controller: 'search',
            access: {
                isLoginRequired: true
            }
        })
        .when('/pairs',
        {
            templateUrl: 'app/views/pairs.html',
            controller: 'pairs',
            access: {
                isLoginRequired: true
            }
        })
        .when('/chat',
        {
            templateUrl: 'app/views/chat-list.html',
            controller: 'chat',
            access: {
                isLoginRequired: true
            }
        })
        .when('/chat/:userId',
        {
            templateUrl: 'app/views/chat-details.html',
            controller: 'chat',
            access: {
                isLoginRequired: true
            }
        })
        .otherwise({ redirectTo: '/error' });

}]);

wzimder.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('httpRequestInterceptor');
}]);

wzimder.run(['$rootScope', '$location', 'Auth', function ($rootScope, $location, Auth) {
    $rootScope.$on('$routeChangeStart', function (event, next, start) {
        if (next.$$route.access != undefined) {
            console.log(next.$$route.originalPath);
            if (next.$$route.access.isLoginRequired) {
                var a = Auth.isUserLoggedIn();
                if (!a) {
                    $location.path('/');
                }
            }
            if (next.$$route.originalPath == '/') {
                if (Auth.isUserLoggedIn()) {
                    $location.path('/dashboard');
                }
            };
        }

    })
}]);