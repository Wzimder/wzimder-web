(function () {
  $('#chat').perfectScrollbar();
  $('#formLog').css('opacity', 1);

    (function(){
        var max = 0,
            i,
            el = $('.eq-wrapper').find('.eq'),
            h;

        for(i=0; i < el.length; i++){
            h = $(el[i]).height();

            if(h > max){
                max = h;
            }
        }

        $(el).each(function(){
            $(this).height(max);
        })
    })();

        $('#navigationBtn').on('click', 'button', function(){
            var $t = $(this),
                    id = $t.prop('id'),
                    btnClass = $t.attr('class'),
                    $legend = $('#legend'),
                    fileName = "",
                    callback,
                    $connector = $('#connect-bttn-form');

            if(btnClass == 'inactive'){
                $t.closest('ul').find('button').removeClass();
                $connector.removeClass();

                if(id == 'btnReg'){
                    $('#formLog')
                            .css('opacity', 0)
                            .remove();
                    fileName = 'index-reg.html';
                    callback = function(){
                        $t.addClass('active');
                        $('#btnLog').addClass('inactive');
                        $connector.addClass('reg');
                        $('#formRegister').css('opacity', 1);
                    }
                }
                else if(id == 'btnLog'){
                    $('#formRegister')
                            .css('opacity', 0)
                            .remove();
                    fileName = 'index-log.html';
                    callback = function(){
                        $t.addClass('active');
                        $('#btnReg').addClass('inactive');
                        $connector.addClass('log');
                        $('#formLog').css('opacity', 1);
                    }
                }
                $.get(fileName, function (data) {
                    $legend.after(data);
                    callback();
                });
            }
        });

        function getCouples() {           
            $.get('couples.html', function (data) {
                $('#couples').append(data);
            });
        }
        $(window).scroll(function () {
            console.log($('.load').visible());
                if ($('.load').visible()) {
                    setTimeout(function () { getCouples(); }, 1000);

                }
            });
 
}());